/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraylist;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author nachorod
 */
public class ArrayListApp {

    public static void main(String[] args) {
        ArrayList<String> nombres=new ArrayList<String>();
        nombres.add("pepe");
        nombres.add("ana");
        nombres.add(1, "nieves");
        for (int i=0; i<nombres.size(); i++) {
            System.out.print(nombres.get(i)+" ");
        }
        System.out.println("");
        Iterator<String> it = nombres.iterator();
        while (it.hasNext()) {
            String nombre=it.next();
            System.out.print(nombre + " ");
        }
        System.out.println("");
    }
    
}
